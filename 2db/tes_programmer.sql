-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 25. April 2016 jam 21:38
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tes_programmer`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `nip` int(30) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `id_posisi` int(2) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`nip`, `nama`, `id_posisi`) VALUES
(123, 'Baznas Kepala', 1),
(456, 'PSDM Baznas', 2),
(789, 'Karyawan Baznas', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `klaim_lembur`
--

CREATE TABLE IF NOT EXISTS `klaim_lembur` (
  `id_klaim` int(5) NOT NULL AUTO_INCREMENT,
  `nip` int(30) NOT NULL,
  `jam_lembur` int(2) NOT NULL,
  `tgl_submit` date NOT NULL,
  PRIMARY KEY (`id_klaim`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `klaim_lembur`
--

INSERT INTO `klaim_lembur` (`id_klaim`, `nip`, `jam_lembur`, `tgl_submit`) VALUES
(1, 123, 2, '2016-04-25'),
(2, 456, 8, '2016-04-25'),
(3, 789, 9, '2016-04-25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE IF NOT EXISTS `posisi` (
  `id_posisi` int(2) NOT NULL AUTO_INCREMENT,
  `nama_posisi` varchar(20) NOT NULL,
  PRIMARY KEY (`id_posisi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`id_posisi`, `nama_posisi`) VALUES
(1, 'Kepala Divisi'),
(2, 'PSDM'),
(3, 'Karyawan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_dana_lembur`
--

CREATE TABLE IF NOT EXISTS `status_dana_lembur` (
  `id_klaim` int(5) NOT NULL,
  `status_proses` int(2) NOT NULL,
  `total_intensif` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_dana_lembur`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `status_lembur`
--

CREATE TABLE IF NOT EXISTS `status_lembur` (
  `id_klaim` int(5) NOT NULL,
  `status_lembur` varchar(10) NOT NULL,
  `nip` int(30) NOT NULL,
  `tgl_status` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_lembur`
--

INSERT INTO `status_lembur` (`id_klaim`, `status_lembur`, `nip`, `tgl_status`) VALUES
(1, 'Approve', 123, '2016-04-25'),
(2, 'New', 456, '0000-00-00'),
(3, 'Approve', 789, '2016-04-25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_proses`
--

CREATE TABLE IF NOT EXISTS `status_proses` (
  `id_status_proses` int(2) NOT NULL AUTO_INCREMENT,
  `nama_status_proses` varchar(20) NOT NULL,
  PRIMARY KEY (`id_status_proses`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `status_proses`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`username`, `password`, `level`) VALUES
('123', '123', 1),
('456', '456', 2),
('789', '789', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
