<html>
	<head></head>
	<body>
		<p><?php echo $title; ?></p>

		<?php 
		if (validation_errors()){
			echo validation_errors();
		} 
		?>

		<form action="<?php echo base_url('klaim_lembur/add') ?>" method="post">
			<table>
				<tr>
					<td>NIP/Nama</td>
					<td>
					   <select name="nip">
				           <option></option>
				           <?php foreach($nip as $rownip){ ?>
				           <option value="<?php echo $rownip->nip?>"><?php echo $rownip->nip."/".$rownip->nama ?></option>
				           <?php } ?>
			           </select>
			        </td>
				</tr>
				<tr>
					<td>Jam Lembur</td>
					<td><input type="text" name="jam_lembur" ></td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Submit">
						<button type="reset" value="Batal">Batal</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>