<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_login');
	}

	public function index()
	{
		$this->load->view('vlogin');
	}

	function aksi_login(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		
		$where = array(
						'username' => $username,
						'password' => $password
						);
		
		$cek = $this->model_login->cek_login('user',$where)->num_rows();
		
		if($cek > 0){

			$rowf = $this->model_login->data_login('user',$where)->row();

		            $data = array(
		                'status' => "login",
		                'username' => $rowf->username,
		                'level' => $rowf->level
		            );

					$this->session->set_userdata($data);
	
					if($rowf->level == '1'){
						redirect(base_url("kepala_divisi")); 
					}elseif ($rowf->level == '2'){
						redirect(base_url("psdm")); 
					}elseif ($rowf->level == '3'){
						redirect(base_url("karyawan")); 
					}else{
						echo "error";
					}

		}else{
			echo "username dan password salah";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login')); //controller login menuju method index
	}



}

?>
