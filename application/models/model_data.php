<?php

	/**
	* 
	*/
	class Model_data extends CI_Model
	{

		function get_nip(){
			$this->db->from('karyawan');
			$query=$this->db->get();

			if($query->num_rows()>0 ){
				return $query->result();
			}else{
				echo "data kosong";
			}
		}

		function get_insert_klaim($data1,$nip){

			$this->db->insert('klaim_lembur', $data1);

			$id_klaim = $this->db->insert_id();
			$q = $this->db->get_where('klaim_lembur', array('id_klaim' => $id_klaim));

	        if($q->num_rows == 1)
	        {
				$rowf = $q->row();
	 			$data2 = array(
	 						'id_klaim' => $rowf->id_klaim,
	 						'nip' => $nip,
	 						'status_lembur' => "New",
	 						'tgl_status' => ""
	 						);

				$this->db->insert('status_lembur',$data2);
				return TRUE;
			}else{
				echo "error";
			}
		}


		function get_data_lembur()
		{
			$this->db->from('karyawan');
			$this->db->join('klaim_lembur','karyawan.nip=klaim_lembur.nip');
			$this->db->join('posisi','karyawan.id_posisi=posisi.id_posisi');
			$this->db->join('status_lembur','klaim_lembur.id_klaim=status_lembur.id_klaim');
			$query=$this->db->get();

			if($query->num_rows()>0){
				return $query->result();
			}else{
				echo "Data tidak ada";
			}
		}

		function get_data_lemburid($id_klaim){
			$this->db->from('karyawan');
			$this->db->join('klaim_lembur','karyawan.nip=klaim_lembur.nip');
			$this->db->join('posisi','karyawan.id_posisi=posisi.id_posisi');
			$this->db->join('status_lembur','klaim_lembur.id_klaim=status_lembur.id_klaim');
			$this->db->where('status_lembur.id_klaim',$id_klaim);

			$query = $this->db->get();
	        return $query->result();
		}

		function get_edit_status($id_klaim,$data){
			$this->db->where('id_klaim', $id_klaim);
			$this->db->update('status_lembur',$data);
			return TRUE;
		}


	}

?>
